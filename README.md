Rclone Emby scanner
======

A Python script which watches an Rclone mount for changes and scans the new directory to Emby.