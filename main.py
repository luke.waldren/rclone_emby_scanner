import os
import sys
import requests

from time import sleep


try:
    log = '/scan_log.txt'
    open(log, 'w+').close()
    log_directory = '/rclone_log.txt'
    emby_url = os.environ['URL']
    emby_key = os.environ['KEY']
except Exception as e:
    print (f"ERROR: {e} ENV NOT FOUND <<TERMINATING>>")
    sys.exit()

def update_log(message):
    with open(log, 'a') as the_file:
        the_file.write(f'{message}\n')

def scan_to_emby(new_file_path):
    url = f"{emby_url}"
    querystring = {"api_key": emby_key}
    payload = {"Updates": [
        {"Path": '/' + new_file_path[1:-1], "UpdateType": "Created"}]}
    headers = {
        'accept': "*/*",
        'Content-Type': "application/json",
        'cache-control': "no-cache",
    }
    request = requests.request(
        "POST", url+'/Library/Media/Updated', headers=headers, params=querystring, json=payload)
    update_log(request)


def get_lines():
    with open(log_directory) as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    clear_file()
    return content


def clear_file():
    open(log_directory, 'w').close()


def find_path(line):
    return (line.split('=')[1].split(',')[0])


def watch_log():
    while True:
        sleep(10)
        for line in get_lines():
            if 'changeNotify: relativePath' in line:
                update_log("Change in mount detected")
                new_file_path = find_path(line)
                scan_to_emby(new_file_path)


if __name__ == '__main__':
    update_log("Watching for changes....")
    clear_file()
    watch_log()
